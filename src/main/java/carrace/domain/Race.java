package carrace.domain;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

public class Race {

	private static final Logger LOG = Logger.getLogger(Race.class);

	private static volatile String winner;

	private int numberOfParticipants;

	public Race(int numberOfParticipants) {
		this.numberOfParticipants = numberOfParticipants;
	}

	public static void setWinner(String car) {
		if (winner == null) {
			synchronized (Race.class) {
				if (winner == null) {
					winner = car;
				}
			}
		}
	}

	public void start() {
		ExecutorService executorService = Executors.newFixedThreadPool(numberOfParticipants);
		Random random = new Random();
		for (int i = 0; i < numberOfParticipants; i++) {
			executorService.execute(new Car("car " + i, random.nextInt(100)));
		}
		executorService.execute(new Referee(numberOfParticipants));
		executorService.shutdown();
		while (!executorService.isTerminated()) {
		}
		LOG.info("Winner is " + winner);
	}
}
