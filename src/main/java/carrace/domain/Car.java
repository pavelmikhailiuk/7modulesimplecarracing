package carrace.domain;

import org.apache.log4j.Logger;

public class Car implements Runnable {

	private static final long MAX_DISTANCE = 10000;

	private static final Logger LOG = Logger.getLogger(Car.class);

	private long friction;
	private long distance;

	private String name;

	public Car(String name, long friction) {
		this.name = name;
		this.friction = friction;
	}

	@Override
	public void run() {
		try {
			while (distance < MAX_DISTANCE && !Thread.currentThread().isInterrupted()) {
				Thread.sleep(friction);
				distance += 100;
				LOG.info(name + " " + distance);
				if (disqualification()) {
					LOG.info(name + " was disqualified");
					Thread.currentThread().interrupt();
				}
			}
			Race.setWinner(name);
		} catch (InterruptedException e) {
			LOG.error(name, e);
		}
	}

	private boolean disqualification() {
		return name.indexOf(String.valueOf(Referee.disqualified)) != -1;
	}
}