package carrace.domain;

import java.util.Random;

import org.apache.log4j.Logger;

public class Referee implements Runnable {

	private static final Logger LOG = Logger.getLogger(Referee.class);

	public static Integer disqualified;

	private int numberOfParticipants;

	public Referee(int numberOfParticipants) {
		this.numberOfParticipants = numberOfParticipants;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(5000);
			disqualified = new Random().nextInt(numberOfParticipants);
			LOG.info("Disqualified: car " + disqualified);
		} catch (InterruptedException e) {
			LOG.error("Referee was awakened too early :)", e);
		}
	}
}
