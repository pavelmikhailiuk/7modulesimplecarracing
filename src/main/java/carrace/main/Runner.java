package carrace.main;

import carrace.domain.Race;

public class Runner {
	public static void main(String[] args) {
		Race race = new Race(10);
		race.start();
	}
}
